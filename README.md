kylin-code fork 自 [Code OSS](https://github.com/microsoft/vscode) 项目，集成了一些 openKylin 的特色插件以及 [openvsx](https://open-vsx.org/) 插件商店。致力于为 openKylin 社区提供集程序编辑、编译、调试、发布、分析等全套开发流程的编程环境，后续将支持C、C++、Java、Go多种等编程语言，以满足 openKylin 平台上的软件开发需求。

## 构建步骤

1. 下载源码：

		git clone https://gitee.com/openkylin/kylin-code.git

2. 安装指定版本nodejs：

	版本要求：>=16.14.x and <17

	获取地址：https://nodejs.org/dist/

3. 安装yarn

	yarn有版本要求，如果未安装或提示版本不符。可以安装1.17.0

	卸载yarn：

		npm uninstall yarn --global

	安装：

		npm install --global yarn@1.17.0

4. C/C++ compiler tool chain

		sudo apt-get update
		sudo apt-get install build-essential g++ libx11-dev libxkbfile-dev libsecret-1-dev

5. npm/yarn 设置

	设置淘宝镜像(解决下载electron慢的问题)

		yarn config set registry http://registry.npm.taobao.org/
		npm config set registry https://registry.npm.taobao.org

	在项目根目录新建文件.npmrc, 在其中添加 (注意，在.npmrc中必须小写，在环境变量中是大写)

		sass_binary_site=https://npm.taobao.org/mirrors/node-sass/
		registry=https://registry.npm.taobao.org
		electron_mirror=https://npm.taobao.org/mirrors/electron/

6. 安装依赖

		cd  kylin-code
		yarn

7. 构建

		yarn watch

8. 运行

		./scripts/code.sh

9. 配置商店地址

	在源码根目录下的 product.json 中添加如下json片段，配置 open vsx 开源插件商店：

	```json
 	"extensionsGallery": {
	"serviceUrl": "https://open-vsx.org/vscode/gallery",
	"itemUrl": "https://open-vsx.org/vscode/item"
  	}
	```
